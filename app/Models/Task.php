<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Note;

class Task extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'note_id', 'tag'];

    public function note()
    {
        return $this->belongsTo(Note::class);
    }
}
