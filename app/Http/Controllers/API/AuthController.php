<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Password;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'registration', 'forgot', 'reset', 'verify']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * User registration
     */
    public function registration()
    {
        $exist = User::where('email', request('email'))->first();

        if ($exist) {
            return response()->json(['error' => 'Mail already exists!']);
        }
        $name = request('name');
        $email = request('email');
        $password = request('password');

        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = Hash::make($password);
        $user->save();

        event(new Registered($user));

        return response()->json(['message' => 'Successfully registration!']);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function verify()
    {
        $user = User::findOrFail(request('id'));
        $user->email_verified_at = date("Y-m-d g:i:s");
        $user->save();

        return redirect("/");
    }

    public function verifyNotice(Request $request)
    {
        $request->user()->sendEmailVerificationNotification();
    }

    public function forgot(Request $request)
    {
        $status = Password::sendResetLink(
            $request->only('email')
        );

        return $status === Password::RESET_LINK_SENT
            ? back()->with(['status' => __($status)])
            : back()->withErrors(['email' => __($status)]);
    }

    public function reset(Request $request)
    {
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) use ($request) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->save();

                $user->setRememberToken(Str::random(60));

                event(new PasswordReset($user));
            }
        );

        return $status == Password::PASSWORD_RESET
            ? redirect()->route('login')->with('status', __($status))
            : back()->withErrors(['email' => [__($status)]]);
    }

    public function updateUser(Request $request)
    {
        $user = User::find($request->id);
        $exist = User::where('email', request('email'))->first();

        if ($exist && $exist != $user) {
            return response()->json(['error' => 'Mail already exists!']);
        }

        $name = request('name');
        $email = request('email');
        $oldPassword = request('oldPassword');
        $newPassword = request('newPassword');

        if ($user->email != $email) {
            $user->email_verified_at = null;
            event(new Registered($user));
        }

        $user->name = $name;
        $user->email = $email;

        if ($newPassword) {
            if (!Hash::check($oldPassword, $user->password)) {
                return response()->json(['error' => 'Wrong old password!']);
            }
            $user->password = Hash::make($newPassword);
        }
        $user->save();

        return response()->json(['message' => 'Successfully update!']);
    }
}
