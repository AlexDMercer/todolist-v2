<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Task;
use App\Models\Note;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(1)->create();
        Note::factory(3)->create()->each(function ($note) {
            $note->tasks()->saveMany(Task::factory(10)->make());
        });
    }
}
