<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\TaskController;
use App\Http\Controllers\API\NoteController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

//Auth Routes
Route::post('login', 'App\Http\Controllers\Api\AuthController@login');
Route::post('registration', 'App\Http\Controllers\Api\AuthController@registration');
Route::post('logout', 'App\Http\Controllers\Api\AuthController@logout');
Route::post('refresh', 'App\Http\Controllers\Api\AuthController@refresh');
Route::get('me', 'App\Http\Controllers\Api\AuthController@me');
Route::post('notice', 'App\Http\Controllers\Api\AuthController@verifyNotice');
Route::get('verify/{id}/{hash}', 'App\Http\Controllers\Api\AuthController@verify')->name('verification.verify');
Route::post('forgot', 'App\Http\Controllers\Api\AuthController@forgot');
Route::post('reset', 'App\Http\Controllers\Api\AuthController@reset');
Route::post('updateUser', 'App\Http\Controllers\Api\AuthController@updateUser');

//Resource Routes
Route::apiResource('notes', NoteController::class);
Route::apiResource('tasks', TaskController::class);
