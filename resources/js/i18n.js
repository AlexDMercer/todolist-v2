import Vue from 'vue';
import VueI18n from 'vue-i18n';
Vue.use(VueI18n);

const messages = {
   en: {
      button: {
         confirm: 'Confirm',
         cancel: 'Cancel',
         edit: 'Edit',
         delete: 'Delete'
      },
      messages: {
         note: {
            title: 'Create new List',
            label: 'List',
            edit: 'Edit list',
            required: 'Please input note name',
            length: 'Maximum length 15',
            created: 'List created',
            updated: 'List updated',
            delete: 'Note deleted',
            select: 'Please, select Task list',
            notify: 'Notify was send, check email'
         },
         task: {
            title: 'Create new Task',
            label: 'Task',
            tag_label: 'Tag',
            edit: 'Edit task',
            required: 'Please input task name',
            length: 'Maximum length 255',
            t_required: 'Please input tag',
            t_length: 'Maximum length 10',
            created: 'Task created',
            updated: 'Task updated',
            delete: 'Task deleted'
         }
      },
      sidebar: {
         actions: 'Action',
         list: 'Add new List',
         task: 'Add new Task',
         lists: 'Task Lists',
         options: 'Options',
         user: 'User options',
         verified: 'Email verified',
         verify: 'Verify email',
         data: 'Change user data',
         app: 'App options',
         dark: 'Dark theme',
         lang: 'Lang',
      },
      navbar: {
         label: 'TaskList',
         home: 'Home',
         info: 'Info',
         hello: 'Hello',
         login: 'Login',
         reg: 'Registration',
         logout: 'Logout',
      },
      tasks: {
         name: 'Name',
         date: 'Date',
         tag: 'Tag',
         search: "Type to search",
         empty: "You have no tasks"
      },
      auth: {
         name: 'Name',
         email: 'Email',
         pass: 'Password',
         confirm: 'Confirm pass',
         name_req: 'Please input name',
         email_req: 'Please input email',
         pass_req: 'Please input password',
         check: 'Please input correct email address',
         exists: 'Mail already exists or wrong old password!',
         wrong: 'Wrong email or password!',
         edit: {
            title: 'Edit user',
            old_pass: 'Old password',
            new_pass: 'New password',
            update: 'Update password',
            message: 'User data has been updated'
         },
         forgot: {
            title: 'Reset password',
            wrong: 'Wrong email!',
            message: 'Message for reset password has been send!'
         },
         login: {
            title: 'Login',
            forgot: 'Forgot password?'
         },
         reg: {
            title: 'Register',
            check: 'Password does not match!',
            confirm: 'Please confirm password',
            exists: 'Mail already exists!',
            verify: 'Please, check your email and verify your email adress'
         },
         reset: {
            title: 'Reset password',
            message: 'Password change successfully'
         }
      }
   },
   ru: {
      button: {
         confirm: 'Готово',
         cancel: 'Отмена',
         edit: 'Редактировать',
         delete: 'Удалить'
      },
      messages: {
         note: {
            title: 'Создать новый список задач',
            label: 'Список',
            edit: 'Редактировать список задач',
            required: 'Введите название списка',
            length: 'Максимальная длина 15',
            created: 'Список создан',
            updated: 'Список обновлен',
            delete: 'Список удален',
            select: 'Выберите список задач',
            notify: 'Уведомление отправлено, проверьте почту'
         },
         task: {
            title: 'Создать новую задачу',
            label: 'Задача',
            tag_label: 'Тэг',
            edit: 'Редактировать задачу',
            required: 'Введите название задачи',
            length: 'Максимальная длина 255',
            t_required: 'Введите тэг',
            t_length: 'Максимальная длина 10',
            created: 'Задача создана',
            updated: 'Задача обновлена',
            delete: 'Задача удалена'
         }
      },
      sidebar: {
         actions: 'Действия',
         list: 'Добавить новый лист',
         task: 'Добавить новую задачу',
         lists: 'Списки задач',
         options: 'Опции',
         user: 'Пользовательские опции',
         verified: 'Почта подтверждена',
         verify: 'Подтвердите почту',
         data: 'Данные пользователя',
         app: 'Опции приложения',
         dark: "Темная тема",
         lang: "Язык"
      },
      navbar: {
         label: 'Список задач',
         home: 'Домой',
         info: 'Инфо',
         hello: 'Привет',
         login: 'Войти',
         reg: 'Зарегистрироваться',
         logout: 'Выйти'
      },
      tasks: {
         name: 'Задача',
         date: 'Дата',
         tag: 'Тэг',
         search: "Поиск задачи",
         empty: "У вас нет задач"
      },
      auth: {
         name: 'Имя',
         email: 'Почта',
         pass: 'Пароль',
         confirm: 'Подтверждение',
         name_req: 'Введите имя',
         email_req: 'Введите почту',
         pass_req: 'Введите пароль',
         check: 'Введите корректный почтовый адрес',
         exists: 'Такая почта уже существует или неверный старый пароль!',
         wrong: 'Неверная почта или пароль!',
         edit: {
            title: 'Редактирование пользователя',
            old_pass: 'Старый пароль',
            new_pass: 'Новый пароль',
            update: 'Редактировать пароль',
            message: 'Данные пользователя отредактированы'
         },
         forgot: {
            title: 'Сброс пароля',
            wrong: 'Неверная почта!',
            message: 'Письмо со ссылкой для сброса пароля отправлено на почту!'
         },
         login: {
            title: 'Логин',
            forgot: 'Забыли пароль?'
         },
         reg: {
            title: 'Регистрация',
            check: 'Пароль не совпадает!',
            confirm: 'Подтвердите пароль',
            exists: 'Почта уже существует!',
            verify: 'Проверьте и подтвердите почту'
         },
         reset: {
            title: 'Сброс пароля',
            message: 'Пароль изменен'
         }
      }
   }
}

export const i18n = new VueI18n({
   locale: 'en',
   messages
})

