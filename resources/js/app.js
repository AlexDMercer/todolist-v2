require('./bootstrap')
require('./store/subscriber');
window.Vue = require('vue').default

import store from './store'
import router from './router'
import moment from 'moment';
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import { i18n } from './i18n'
//import 'element-ui/lib/theme-chalk/index.css'

Vue.use(ElementUI, { locale });
Vue.prototype.$moment = moment;

store.dispatch('attempt', localStorage.getItem('token')).then(() => {
    const app = new Vue({
        el: '#app',
        router, store, i18n
    });
})
