import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import tasks from './tasks'
import notes from './notes'
import auth from './auth'
Vue.use(Vuex)

const store = new Vuex.Store({
   modules: {
      tasks: tasks,
      notes: notes,
      auth: auth
   }
})

export default store