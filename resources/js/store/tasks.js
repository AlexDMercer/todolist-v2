const tasks = {
    state: {
        tasks: []
    },
    getters: {
        Tasks(state) {
            return state.tasks
        }
    },
    actions: {
        getTasks({ commit }, id) {
            axios.get(" api/notes/" + id, { id }).then(res => {
                commit('setTasks', res.data)
            })
                .catch(error => console.log(error))
        },
        addTask({ commit }, task) {
            axios.post("api/tasks", task).then(res => {
                commit('newTask', res.data)
            })
        },
        updateTask({ commit }, task) {
            axios.put("api/tasks/" + task.id, task).then(res => {
                commit('upTask', res.data)
            })
        },
        delTask({ commit }, task) {
            axios.delete("api/tasks/" + task.id).then(res => {
                commit('removeTask', task)
            })
        }
    },
    mutations: {
        setTasks: (state, tasks) => {
            state.tasks = tasks
        },
        newTask: (state, task) => {
            state.tasks.push(task)
        },
        upTask: (state, task) => {
            return
        },
        removeTask: (state, task) => {
            let index = state.tasks.findIndex(item => item.id === task.id)
            state.tasks.splice(index, 1)
        }
    }
}

export default tasks
