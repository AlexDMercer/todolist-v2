const auth = {
   state: {
      token: null,
      user: null,
      resp: null
   },

   getters: {
      authenticated(state) {
         return state.token && state.user
      },
      user(state) {
         return state.user
      },
      resp(state) {
         return state.resp
      }
   },

   actions: {
      async reg({ commit, dispatch }, form) {
         await axios.post("api/registration", form).then(res => {
            commit('setResp', res.data)
            if (!res.data.error) {
               dispatch('signIn', form)
            }
         });
      },

      async updateUser({ commit }, form) {
         console.log(form);
         await axios.post("api/updateUser", form).then(res => {
            commit('setResp', res.data)
         })
      },

      async signIn({ dispatch }, form) {
         let res = await axios.post("api/login", form).then(res => {
            dispatch('attempt', res.data.token)
         });
      },

      async attempt({ commit, state }, token) {
         if (token) {
            commit('setToken', token)
         }
         if (!state.token) {
            return
         }

         try {
            let response = await axios.get("api/me")
            commit('setUser', response.data)
         } catch (e) {
            console.log('failed');
            commit('setToken', null)
            commit('setUser', null)
         }
      },

      logout({ commit }) {
         return axios.post('api/logout').then(() => {
            commit('setToken', null)
            commit('setUser', null)
         })
      },

      sendNotice({ commit }, user) {
         axios.post("api/notice", user)
      },

      forgotPass({ commit }, form) {
         axios.post("api/forgot", form)
      },

      reset({ dispatch }, form) {
         axios.post("api/reset", form)
         dispatch('signIn', form)
      },
   },

   mutations: {
      setToken(state, token) {
         state.token = token
      },
      setUser(state, data) {
         state.user = data
      },
      setResp(state, resp) {
         state.resp = resp
      },
   }
}

export default auth