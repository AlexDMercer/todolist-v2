const notes = {
   state: {
      notes: []
   },
   getters: {
      Notes(state) {
         return state.notes
      }
   },
   actions: {
      getNotes({ commit }) {
         axios.get("api/notes").then(res => {
            commit('setNotes', res.data.notes)
         })
            .catch(error => console.log(error))
      },
      addNote({ commit }, name) {
         axios.post("api/notes", { name: name }).then(res => {
            commit('newNote', res.data)
         })
      },
      updateNote({ commit }, note) {
         axios.put("api/notes/" + note.id, notes).then(res => {
            commit('upTask', res.data)
         })
      },
      delNote({ commit }, note) {
         axios.delete("api/notes/" + note.id).then(res => {
            commit('removeNote', note)
         })
      }
   },
   mutations: {
      setNotes: (state, notes) => {
         state.notes = notes
      },
      newNote: (state, note) => {
         state.notes.push(note)
      },
      upNote: (state, note) => {
         return
      },
      removeNote: (state, note) => {
         let index = state.notes.findIndex(item => item.id === note.id)
         state.notes.splice(index, 1)
      }
   }
}

export default notes