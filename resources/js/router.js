import Vue from 'vue'
import vueRouter from 'vue-router'
Vue.use(vueRouter)

import store from './store'
import app from './components/app'
import tasks from './components/tasks'
import info from './components/info'
import reset from './components/auth/reset'
import nopage from './components/nopage'

export default new vueRouter({
    mode: "history",
    routes: [
        {
            path: '/',
            component: app,
            name: 'app',
            children: [
                {
                    path: 'List-:id',
                    components: { tasks: tasks },
                    name: 'tasks',
                    beforeEnter: (to, from, next) => {
                        window.MessageEvent
                        if (!store.getters['authenticated']) {
                            next({ name: 'app' })
                            alert('Login first please!')
                        } else next()
                    }
                },
                {
                    path: 'info',
                    components: { info: info },
                    name: 'info',
                    beforeEnter: (to, from, next) => {
                        window.MessageEvent
                        if (!store.getters['authenticated']) {
                            next({ name: 'app' })
                            alert('Login first please!')
                        } else next()
                    }
                },
            ]
        },
        {
            path: '/reset-*',
            component: reset,
            name: 'reset'
        },
        {
            path: '*',
            component: nopage
        },
    ]
})
